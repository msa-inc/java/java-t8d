---
title: Lien « est-un » ou « a-un »
author: Antoine DE OLIVIERA, Maxime TUEUX, Sam VIE
---

# TD 10 – Lien « est-un » ou « a-un »

```yaml
 RSI 2A :
  - Antoine DE OLIVEIRA
  - Maxime TUEUX
  - Sam VIE
```

[TOC]

## 1. Quel lien choisir ?

### 1.1. Avec un lien « est-un »

#### *Donner une ébauche du diagramme de classes montrant où sont réalisées les différentes méthodes.*

```mermaid
classDiagram
	class PassagerAbstrait {
		<<abstract>>
		#faireChoixArret(t~Autobus~, numeroArret~int~, destination~int~)*
	}
	class Montee {
		<<abstract>>
		+monterDans(t~Autobus~)
		#faireChoixArret(t~Autobus~, numeroArret~int~, destination~int~)*
	}
	class Arret {
		#faireChoixArret(t~Autobus~, numeroArret~int~, destination~int~)
	}
	
	PassagerAbstrait <|-- Montee
	Montee <|-- Arret
```

#### *Combien faut-il construire de classes concrètes pour effectuer les combinaisons ?* *En quoi cette solution n’est-elle pas satisfaisante ?*

Dans cette optique, nous serions obligés de prévoir toutes les combinaisons possibles de classes. Cela donne $4 \cdot 5 = 20$ classes concrètes. On pourrait envisager de mettre en place une génération de code, cependant cela ne résoudrait que partiellement le problème en plus d’être excessivement complexe. 

De plus, l’ajout de nouveau comportements deviendrait trop complexe pour que cette solution soit adoptée. En un mot, cette solution n’offre pas assez de modularité, de facilité de maintenance et de capacité d’évolution pour être consiédérée.

### 1.2. Avec un lien « a-un »

#### *Fournir le diagramme de classes de cette solution.*

```mermaid
classDiagram
	class PassagerAbstrait {
		<<abstract>>
		+monterDans(t~Autobus~)*
	}
	class MonteeFatigue {
		+monterDans(t~Autobus~)
	}
	class MonteeTetu {
		+monterDans(t~Autobus~)
	}
	class MonteeRepos {
		+monterDans(t~Autobus~)
	}
	class MonteeSportif {
		+monterDans(t~Autobus~)
	}
	class Arret {
		<<interface>>
		+faireChoixArret(p~PassagerAbstrait~, numeroArret~int~, distanceDestination~int~)
	}
	class ArretCalme {
	}
	class ArretNerveux {
	}
	class ArretPrudent {
	}
	class ArretAgoraphobe {
	}
	class ArretPoli {
	}
	
	PassagerAbstrait "1" --> "1" Arret
	Arret <|-- ArretCalme
	Arret <|-- ArretNerveux
	Arret <|-- ArretPrudent
	Arret <|-- ArretAgoraphobe
	Arret <|-- ArretPoli
	PassagerAbstrait <|-- MonteeFatigue
	PassagerAbstrait <|-- MonteeTetu
	PassagerAbstrait <|-- MonteeRepos
	PassagerAbstrait <|-- MonteeSportif
```

#### *Expliquer pourquoi l’utilisation d’un lien « a-un » peut permettre d’éviter la multiplication du nombre de classes induite par la combinaison des caractères.*

En utilisant un lien « a-un », on évite la double dimension d’héritage présentée en section 1.1 qui entraîne un si grand nombre de classes concrètes.

## 2. Développement du lien « a-un »

### 2.1. La hiérarchie pour la méthode `choixPlaceArret()`[^1]

[^1]: Pour correspondre à notre nomenclature, cette méthode a été renommée `faireChoixArret()`.

#### *En terme d’objets, donner la différence entre les liens « a-un » et « est-un » qui explique l’ajout de ce paramètre.*

Un lien *“a-un”* est une association de deux objets à travers une ou deux instances (mono ou bi-directionnelle) que l’un possède de l’autre. Ainsi, si l’on a un lien *“a-un”* entre un `Passager` et un `Arret`, chaque instance du premier contiendra un attribut stockant une instance du second.

À l’inverse, un lien *“est-un”* est en somme une extension d’une classe : on intègre l’ensemble de son fonctionnement et on l’étend pour le faire convenir à l’usage spécifique de la classe fille (d’où l’utilisation du mot-clé `extends`). Dans le langage courant, on parlera d’héritage.

Ainsi, étant donné qu’`Arret` n’hérite pas de `PassagerAbstrait` suite aux raisons invoquées section 1.2, cette classe ne bénéficie d’aucun accès aux méthodes et attributs de `PassagerAbstrait`. Il est donc nécessaire de la doter d’une nouvelle méthode prenant en paramètres tous les éléments nécessaires à son fonctionnement, ceci à fin de permettre à la classe qui lui est associée d’utiliser son comportement.

### 2.2. La classe abstraite et ses dérivés

#### *Comment se traduit ce lien « a-un » dans le code ?*

Ce lien se traduit d’abord par l’ajout d’un attribut dans la classe abstraite, appelée à contenir une instance de l’objet cible. Ensuite, dans les méthodes où l’on a besoin de faire appel au fonctionnement de la classe associée, on l’appellera ainsi :

```java
    public void nouvelArret(Autobus bus, int numeroArret) {
        int distanceDestination = destination - numeroArret;
        this.characterArret.faireChoixArret(this, bus, distanceDestination);
        // => On passe par l'instance pour appeler la méthode, qui prend tout ce dont 
        //    elle a besoin en paramètres, notamment l'instance du passager.
    }
```

### 2.3. Mais où se fait l’instanciation des classes `Arret*` ?

> **Toutes les instanciations des classes `Arret*` sont effectuées dans le constructeur de la classe `PassagerAbstrait`. Le constructeur prend un paramètre (par exemple un entier) indiquant quelle classe doit être instanciée.** 

#### *Donner le code de ce constructeur.*

```java
    public PassagerAbstrait(String nom, int destination, String typeArret) {
        ...
        
        // On est obligés de vérifier à l'aide d'un `switch … case` le type d'arret
        // à instancier, et de gérer son instanciation manuellement.
        switch (typeArret) {
            case "agoraphobe":
                this.characterArret = new ArretAgoraphobe();
             	break;
            case "calme":
                this.characterArret = new ArretCalme();
             	break;
            case "nerveux":
                this.characterArret = new ArretNerveux();
             	break;
            case "poli":
                this.characterArret = new ArretPoli();
             	break;
            case "prudent":
                this.characterArret = new ArretPrudent();
             	break;
        }
    }
```

#### *Conclusion.*

Cette solution pose des problèmes de modularité et de maintenabilité (notamment en cas d’ajout d’un nouveau comportement d’arrêt), il vaudrait mieux privilégier la création d’une fabrique pour les instances d’`Arret`.

> **Les combinaisons sont créées à partir des classes dérivées.**
> **L’instanciation de chaque classe dérivée de `PassagerAbstrait` instancie une et une seule des classes `Arret*` puis passe cette instance au constructeur de la classe `PassagerAbstrait`.**

#### *Conclusion.*

Cela déplace le problème sur les classes dérivées sans pour autant le résoudre. Il est même fortement probable que cela crée plus encore de duplication de code.

> Les combinaisons sont créées par la fabrique.
> L’instanciation des classes `Arret*` se fait dans la fabrique et cette instance est passée en paramètre à l’instanciation d’une classe dérivée de `PassagerAbstrait`.

#### *Conclusion.*

L’utilisation d’une fabrique permet d’éviter la problématique de répétition de code : on remplace la gestion du switch … case par le choix d’une méthode de la fabrique à utiliser pour générer la bonne instance. Cela implique une meilleure maintenabilité en cas d’ajout ou de suppression d’un cas d’usage, et offre en bonus la possibilité d’effectuer des tests unitaires sur cette opération de création le cas échéant.

> Autre manière : créer les combinaisons dans le programme principal (la classe `Simple`).

#### *Donner la version de la fabrique qui permet cette construction.*

```java
public class ArretFactory {

    /*
     * Chaque type d'arrêt est un singleton partagé par
     * toutes les instances de "MonteeXXXX". Cela est possible
     * car les objets de type Arret sont sans états.
     */
    public static Arret agoraphobe = new ArretAgoraphobe();
    public static Arret calme = new ArretCalme();
    public static Arret nerveux = new ArretNerveux();
    public static Arret prudent = new ArretPrudent();
    public static Arret poli = new ArretPoli();

    public static Arret createArretAgoraphobe() {
        return agoraphobe;
    }

    public static Arret createArretCalme() {
        return calme;
    }

    public static Arret createArretNerveux() {
        return nerveux;
    }

    public static Arret createArretPrudent() {
        return prudent;
    }

    public static Arret createArretPoli() {
        return poli;
    }

}
```

#### *Expliquer comment respecter les contraintes de portée des méthodes pour les classes `Arret*` ?*

Nous n’avons pas bien compris cette question, dans la mesure où nous avons laissé une portée publique à l’ensemble des classes `Arret*` et à leurs méthodes. Nonobstant ce fait, on peut légitimement se demander si c’est nécessaire, dans la mesure où c’est la fabrique qui instancie ces classes. Un portée de type paquetage est probablement plus adaptée pour les classes. 

Pour ce qui est des méthodes, nous avons fait le choix délibéré de conserver une portée publique, afin d’en autoriser l’accès depuis un autre paquetage si cela s’avérait nécessaire par la suite.