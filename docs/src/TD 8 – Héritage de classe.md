---
title: Classe abstraite et Factorisation
author: Antoine DE OLIVIERA, Maxime TUEUX, Sam VIE
---

# TD 8 – Héritage de classe

```yaml
 RSI 2A :
  - Antoine DE OLIVEIRA
  - Maxime TUEUX
  - Sam VIE
```

[TOC]

## 1. La classe concrète **`PassagerStandard`**

#### *Quelles méthodes sont à redéfinir dans les classes dérivées pour minimiser le code dupliqué ?*

Les méthodes à redéfinir sont :

- `monterDans()`
- `nouvelArret()`

#### *Dans ces méthodes, comment réutiliser, si nécessaire, le code de la classe de base ?*

À l’aide de la méthode `super()`, qui exécutera la méthode correspondante de la classe de base.

####*De quelle manière faut-il mieux modifier la classe* **`PassagerStandard`** *pour fournir l’information sur la destination aux classes dérivées ? Expliquez pourquoi.*

Il faut modifier la portée de l’attribut `destination` en `protected`, afin qu’il soit accessible par ses classes dérivées, y compris à l’extérieur du paquetage.

####*Quel est la liste des paramètres du constructeur des classes dérivées ?*

Les paramètres du constructeur des classes dérivées est :

- `String nom`
- `int destination`

####*Quel est le code dans ce constructeur ?*

```java
   // Constructeur de la classe PassagerStresse
   public PassagerStresse (String nom, int destination) {
       super(nom, destination);
   }

   // Constructeur de la classe PassagerLunatique
   public PassagerLunatique (String nom, int destination) {
       super(nom, destination);
   }
```

## 2. Changement du comportement de `PassagerStandard`

#### *Que faut-il modifier dans le code ?*

Il faut modifier deux méthodes de `PassagerStandard` :

- `monterDans()`
- `nouvelArret()`

Du fait que `PassagerStresse` et `PassagerLunatique` redéfinissent les deux méthodes, les classes dérivées ne sont pas impactées par la modification de `PassagerStandard`.

#### *Que conclure sur l’indépendance du comportement de* `PassagerStresse` *et* `PassagerLunatique` *par rapport à celui de* `PassagerStandard` *?*

`PassagerStresse` et `PassagerLunatique` sont indépendants du fonctionnement de leur classe de base quand il s’agit des deux méthodes citées à la réponse précédente, du fait que ces deux classes dérivées les redéfinissent. 

Du reste, les autres méthodes sont héritées de la classe de base, les classes dérivées sont donc tributaires du fonctionnement de la classe de base concernant celles-ci.

## 3. Boutez vos neurones

#### *Expliquer le problème rencontré dans cette situation avec la réutilisation par imbrication (lien « a-un ») par rapport à la réutilisation par inclusion (lien « est-un »)*

On remarque que le `PassagerAnxieux` adapté pour avoir le même comportement que le `PassagerLunatique`, ne change pas de position à chaque arrêt, contrairement à ce dernier.

Le problème se situe dans l’essence même de la relation liant ces deux classes dérivées à leur classe de base. En effet, dans le cas d’un lien de type « a-un », comme c’est le cas avec le `PassagerAnxieux`, deux instances de classe sont crées : une instance de `PassagerStandard` et une instance de `PassagerAnxieux`. Quand nous faisons appel à la méthode `monterDans()` issue de `PassagerStandard`, c’est en réalité la méthode de l’instance stockée de `PassagerStandard` qui est exécutée, ce qui implique que c’est un objet de type `PassagerStandard` qui “monte” dans l’autobus.

À chaque arrêt, c’est donc la méthode `nouvelArret()` de `PassagerStandard`, non-redéfinie par `PassagerAnxieux`, qui est exécutée. Le comportement du passager est donc celui d’un passager standard.

À contrario, un lien de type « est-un » ne crée qu’une seule instance de `PassagerLunatique`, ce qui évite tout problème de ce type, puisque dans ce cas, cela élimine le risque d’une perte d’identité au sein de la méthode `monterDans()`.