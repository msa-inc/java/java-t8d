---
title: Classe abstraite et Factorisation
author: Antoine DE OLIVIERA, Maxime TUEUX, Sam VIE
---

# TD 9 – Classe abstraite et Factorisation

```yaml
 RSI 2A :
  - Antoine DE OLIVEIRA
  - Maxime TUEUX
  - Sam VIE
```

[TOC]

## 1. La classe abstraite `PassagerAbstrait`

#### *Pourquoi toutes les classes dérivées de `PassagerAbstrait` appartiennent-elles forcément au paquetage `tec` ?*

Parce que `PassagerAbstrait` a une portée strictement interne.

####*Pourquoi utiliser la portée `protected` pour la méthode `sortirADestination()` ?*

Fondamentalement, la portée paquetage est suffisante à assurer le bon fonctionnement de la classe. Cependant, dans un but de documentation, on utilise la portée `protected` afin d’afficher de manière ostensible le fait que la méthode est à destination de ses enfants.

#### *Pourquoi la construction de cette classe est obligatoirement une classe abstraite ?*

La classe `PassagerAbstrait` inclut l’interface `Passager`, qui implémente deux méthodes. Or, la classe `PassagerAbstrait` ne définit pas le code de ces deux méthodes, qui seront implémentées par ses classes dérivées. Il ne peut donc s’agit que d’une classe abstraite, autrement Java lèvera une exception à la compilation.

### 1.1. Remanier les classes caractères

#### *Pourquoi n’est-il pas possible d’assurer que tous les passagers sortent à destination ?*

Le fait que la méthode `sortirADestination()` soit appelé dépend entièrement de l’implémentation que chaque classe fille fera de la méthode `nouvelArret()`. Ainsi, rien ne les oblige à utiliser la méthode `sortirADestination()`, on ne peut donc garantir qu’elle sera effectivement utilisée.

Pour pallier à cela, il faut mettre en place un *Patron de méthode* (*Template Method*).

```java
  // Template Method Design Pattern
  public final void nouvelArret(Autobus t, int numeroArret) {
    this.faireChoixArret(t, numeroArret, destination);
  }

  protected abstract void faireChoixArret(Autobus t, int numeroArret, int destination);
```

## 2. Définir ce qu’il faut paramétrer

### 2.1. La classe `PassagerAbstrait`

#### *Quelle portée faut-il choisir pour la méthode `faireChoixArret()` de la classe `PassagerAbstrait` ?*

On choisit la portée `protected`, afin que soit indiquée clairement la volonté de redéfinir la méthode par une classe dérivée.

#### *Expliquer le choix des paramètres de la méthode `faireChoixArret()`.*

On repasse tout d’abord les deux paramètres de la méthode `nouvelArret()`, `Autobus t` et `int numeroArret`, afin de laisser à la classe dérivée des options de personnalisation décentes. Ensuite, afin de ne pas avoir à se préoccuper des attributs de la classe, on ajoute un argument `int destination`.

#### *Comment éviter la redéfinition de la méthode `nouvelArret()` dans les classes dérivées ?*

On ajoute le mot-clé `final`, qui empêche la réécriture de la méthode.
