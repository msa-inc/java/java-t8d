# PG214 — Programmation Orientée Objet — TD 8 à 10

[![](https://img.shields.io/badge/-GitLab-555?logo=gitlab&logoColor=orange)](https://gitlab.com/msa-inc/java/java-t8d)
[![](https://img.shields.io/badge/Java-17.0.1-blue?logo=java)](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)

## Auteurs

- Antoine DE OLIVEIRA
- Maxime TUEUX
- Sam VIE

## Organisation du dépôt

Le code source est disponible sous `/src`.
⚠️ Le paquetage porte le nom `rsi` en lieu et place de `tec`.

Les compte-rendus de TD sont disponibles sous `/docs`.
Un compte-rendu récapitulant l'ensemble des trois TDs est également disponible 
à la racine du dépôt, sous le nom `report.pdf`.

Vous pouvez également naviguer entre les différentes versions du 
code à l'aide de tags placés sur les commits finales de chaque TD.
