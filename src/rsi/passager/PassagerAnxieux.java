/*package rsi.passager;

import rsi.Autobus;
import rsi.Usager;

public class PassagerAnxieux implements Passager, Usager {

    private PassagerStandard p;
    private int destination;

    public PassagerAnxieux(String nom, int destination) {
        this.p = new PassagerStandard(nom, destination);
        this.destination = destination;
    }

    public String nom() {
        return this.p.nom();
    }

    public boolean estDehors() {
        return this.p.estDehors();
    }

    public boolean estAssis() {
        return this.p.estAssis();
    }

    public boolean estDebout() {
        return this.p.estDebout();
    }

    public void changerEnDehors() {
        this.p.changerEnDehors();
    }

    public void changerEnAssis() {
        this.p.changerEnAssis();
    }

    public void changerEnDebout() {
        this.p.changerEnDebout();
    }

    public void nouvelArret(Autobus t, int numeroArret) {
        if (numeroArret == destination - 1) {
            if (estAssis()) {
                t.arretDemanderAssis(this.p);
            } else if (estDebout()) {
                t.arretDemanderDebout(this.p);
            }
        }
    }

    public void monterDans(Autobus t) {
        if (t.aPlaceDebout()) {
            t.monteeDemanderDebout(this.p);
            changerEnDebout();
        } else {
            System.out.println("Ce bus est plein.");
        }
    }

    public String toString() {
        return this.p.toString();
    }
}
*/