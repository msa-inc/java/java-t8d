package rsi.passager;

import rsi.Autobus;
import rsi.arret.Arret;

public class PassagerStandard extends PassagerAbstrait {

    public PassagerStandard(String nom, int destination, Arret arret) {
        super(nom, destination, arret);
    }

    public void monterDans(Autobus t) {
        if (t.aPlaceAssise()) {
            t.monteeDemanderAssis(this);
            changerEnAssis();
        } else if (t.aPlaceDebout()) {
            t.monteeDemanderDebout(this);
            changerEnDebout();
        } else {
            System.out.println("Ce bus est plein.");
        }
    }


}
