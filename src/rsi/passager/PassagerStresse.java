package rsi.passager;

import rsi.Autobus;
import rsi.arret.Arret;

public class PassagerStresse extends PassagerAbstrait {

    public PassagerStresse(String nom, int destination, Arret arret) {
        super(nom, destination, arret);
    }

    public void monterDans(Autobus t) {
        if (t.aPlaceAssise()) {
            t.monteeDemanderAssis(this);
            this.changerEnAssis();
        } else {
            System.out.println("Ce bus est plein.");
        }
    }

}