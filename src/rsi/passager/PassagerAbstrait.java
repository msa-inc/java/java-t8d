package rsi.passager;

import rsi.Autobus;
import rsi.Position;
import rsi.Usager;
import rsi.arret.Arret;


public abstract class PassagerAbstrait implements Passager, Usager {

    private String nom;
    private Position position;
    protected int destination;
    protected Arret characterArret;
    
    public PassagerAbstrait(String nom, int destination, Arret arret) {
        this.nom = nom;
        this.destination = destination;
        this.position = Position.create();
        this.characterArret = arret;
    }

    public String toString() {
        return nom() + " <" + position + ">";
    }

    public String nom() {
        return nom;
    }

    public boolean estDehors() {
        return this.position.estDehors();
    }

    public boolean estAssis() {
        return this.position.estAssis();
    }

    public boolean estDebout() {
        return this.position.estDebout();
    }

    public void changerEnDehors() {
        this.position = Position.dehors();
    }

    public void changerEnAssis() {
        this.position = Position.assis();
    }

    public void changerEnDebout() {
        this.position = Position.debout();
    }

    protected void sortirADestination(Autobus bus, int numeroArret) {
        if (numeroArret == destination) {
            if (estAssis()) {
                bus.arretDemanderAssis(this);
            } else if (estDebout()) {
                bus.arretDemanderDebout(this);
            }
        }
    }

    public void nouvelArret(Autobus bus, int numeroArret) {
        int distanceDestination = destination - numeroArret;
        characterArret.faireChoixArret(this, bus, distanceDestination);
    }

}
