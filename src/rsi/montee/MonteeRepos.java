package rsi.montee;

import rsi.passager.PassagerAbstrait;
import rsi.Autobus;
import rsi.arret.Arret;

public class MonteeRepos extends PassagerAbstrait {

    public MonteeRepos(String nom, int destination, Arret characterArret) {
        super(nom, destination, characterArret);
    }

    public void monterDans(Autobus t) {
        if (t.aPlaceAssise()) {
            t.monteeDemanderAssis(this);
            this.changerEnAssis();
        } else if (t.aPlaceDebout()) {
            t.monteeDemanderDebout(this);
            this.changerEnDebout();
        } else {
            System.out.println("Ce bus est plein.");
        }
    }

}
