package rsi.montee;

import rsi.passager.PassagerAbstrait;
import rsi.Autobus;
import rsi.arret.Arret;

public class MonteeFatigue extends PassagerAbstrait {

    public MonteeFatigue(String nom, int destination, Arret characterArret) {
        super(nom, destination, characterArret);
    }

    public void monterDans(Autobus t) {
        if (t.aPlaceAssise()) {
            t.monteeDemanderAssis(this);
            this.changerEnAssis();
        } else {
            System.out.println("Ce bus est plein.");
        }
    }

}
