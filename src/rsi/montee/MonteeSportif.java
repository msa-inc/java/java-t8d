package rsi.montee;

import rsi.Autobus;
import rsi.arret.Arret;
import rsi.passager.PassagerAbstrait;

public class MonteeSportif extends PassagerAbstrait {

    public MonteeSportif(String nom, int destination, Arret characterArret) {
        super(nom, destination, characterArret);
    }

    public void monterDans(Autobus t) {
        if (t.aPlaceDebout()) {
            t.monteeDemanderDebout(this);
            this.changerEnDebout();
        } else {
            System.out.println("Ce bus est plein.");
        }
    }

}