package rsi.montee;

import rsi.Autobus;
import rsi.arret.Arret;
import rsi.passager.PassagerAbstrait;

public class MonteeTetu extends PassagerAbstrait {

    public MonteeTetu(String nom, int destination, Arret characterArret) {
        super(nom, destination, characterArret);
    }

    public void monterDans(Autobus t) {
        t.monteeDemanderDebout(this);
        this.changerEnDebout();
    }
}
