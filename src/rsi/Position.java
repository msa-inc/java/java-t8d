package rsi;

public class Position {

  private static final int DEHORS = 1;
  private static final int ASSIS  = 2;
  private static final int DEBOUT = 3;
  
  private final int COURANT;

  private static final Position dehors = new Position(DEHORS);
  private static final Position assis = new Position(ASSIS);
  private static final Position debout = new Position(DEBOUT);

  public static Position create() {
    return Position.dehors;
  }

  // Constructeur privé
  private Position(int position) { 
    this.COURANT = position;
  }


  public boolean estDehors() {
    return this == Position.dehors;
  }

  public boolean estAssis() {
    return this == Position.assis;
  }

  public boolean estDebout() {
    return this == Position.debout;
  }

  public boolean estInterieur() {
    return !estDehors();
  }

  // Singleton Position(ASSIS)
  public static Position assis() {
    return Position.assis; 
  }

  // Singleton Position(DEBOUT)
  public static Position debout() {
    return Position.debout;
  }

  // Singleton Position(DEHORS)
  public static Position dehors() {
    return Position.dehors;
  }

  @Override
  public String toString() {
    String nom = null;
    switch(COURANT) {
    case DEHORS :
      nom = "endehors";
      break;
    case ASSIS :
      nom = "assis";
      break;
    case DEBOUT :
      nom = "debout";
      break;
    }
    return "<" + nom + ">";
  }
  
}
