package rsi;

import java.util.ArrayList;

import rsi.passager.Passager;

public class Autobus {

  private final ArrayList<Passager> passagers;
  private final Jauge jaugeAssis;
  private final Jauge jaugeDebout;

  private int numeroArret;

  public Autobus(int nbPlaceAssise, int nbPlaceDebout) {
    this.jaugeAssis = new Jauge(nbPlaceAssise, 0);
    this.jaugeDebout = new Jauge(nbPlaceDebout, 0);
    this.numeroArret = 0;
    this.passagers = new ArrayList<Passager>();
  }


  public String toString() {
    return "[arret:" + this.numeroArret + ", assis: " + this.jaugeAssis + ", debout: " + this.jaugeDebout + "]";
  }


  public boolean aPlaceAssise() {
    return jaugeAssis.estVert();
  }


  public boolean aPlaceDebout() {
    return jaugeDebout.estVert();
  }


  public void monteeDemanderAssis(Passager p) {
    if(jaugeAssis.estVert()) {
      jaugeAssis.incrementer();
      passagers.add(p);
      p.changerEnAssis();
    } else {
      System.out.println("Plus de places assises.");
    }
  }


 public void monteeDemanderDebout(Passager p) {
    if(jaugeDebout.estVert()) {
      jaugeDebout.incrementer();
      passagers.add(p);
      p.changerEnDebout();
    } else {
      System.out.println("Plus de places debouts.");
    }
  }


  public void allerArretSuivant() {
    this.numeroArret++;

    ArrayList<Passager> copie = new ArrayList<>(this.passagers);

    for (Passager p: copie) {
      p.nouvelArret(this, this.numeroArret);
    }
  }


  public void arretDemanderAssis(Passager p) {
    jaugeAssis.decrementer();
    arretDemanderSortie(p);
  }

  public void arretDemanderDebout(Passager p) {
    jaugeDebout.decrementer();
    arretDemanderSortie(p);
  }

  public void arretDemanderSortie(Passager p) {
    passagers.remove(p);
    p.changerEnDehors();  
  }
  
}
