package rsi.arret;

import rsi.Autobus;
import rsi.passager.PassagerAbstrait;

public class ArretNerveux implements Arret {

    public void faireChoixArret(PassagerAbstrait passager, Autobus bus, int distanceDestination) {
        if (distanceDestination == 0) {
            if (passager.estAssis()) {
                bus.arretDemanderAssis(passager);
            } else if (passager.estDebout()) {
                bus.arretDemanderDebout(passager);
            }
        } else if (passager.estAssis()) {
            passager.changerEnDebout();
        } else {
            passager.changerEnAssis();
        }
    }

}