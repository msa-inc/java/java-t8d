package rsi.arret;

import rsi.Autobus;
import rsi.passager.PassagerAbstrait;

public class ArretPrudent implements Arret {

    public void faireChoixArret(PassagerAbstrait p, Autobus b, int distanceDestination) {
        if (distanceDestination == 0) {
            if (p.estAssis()) {
                b.arretDemanderAssis(p);
            } else if (p.estDebout()) {
                b.arretDemanderDebout(p);
            }
        } else if (distanceDestination < 3) {
            p.changerEnDebout();
        } else {
            p.changerEnAssis();
        }
    }

}
