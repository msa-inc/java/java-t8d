package rsi.arret;

import rsi.passager.PassagerAbstrait;
import rsi.Autobus;

public class ArretAgoraphobe implements Arret {

    public void faireChoixArret(PassagerAbstrait passager, Autobus bus, int distanceDestination) {
        if (distanceDestination == 0 || (!bus.aPlaceDebout() && !bus.aPlaceDebout())) {
            if (passager.estAssis()) {
                bus.arretDemanderAssis(passager);
            } else if (passager.estDebout()) {
                bus.arretDemanderDebout(passager);
            }
        }
    }

}
