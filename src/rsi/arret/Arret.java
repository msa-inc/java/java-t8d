package rsi.arret;

import rsi.Autobus;
import rsi.passager.PassagerAbstrait;

public interface Arret {
    public void faireChoixArret(PassagerAbstrait p, Autobus b, int distanceDestination);
}