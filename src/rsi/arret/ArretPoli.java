package rsi.arret;

import rsi.Autobus;
import rsi.passager.PassagerAbstrait;

public class ArretPoli implements Arret {

    @Override
    public void faireChoixArret(PassagerAbstrait p, Autobus b, int distanceDestination) {
        if (distanceDestination == 0) {
            if (p.estAssis()) {
                b.arretDemanderAssis(p);
            } else if (p.estDebout()) {
                b.arretDemanderDebout(p);
            }
        } else if (!b.aPlaceAssise() && p.estAssis()) {
            p.changerEnDebout();
        }
    }

}
