package rsi.arret;

public class ArretFactory {

    /*
     * Chaque type d'arrêt est un singleton partagé par
     * toutes les instances de "MonteeXXXX". Cela est possible
     * car les objets de type Arret sont sans états.
     */
    public static Arret agoraphobe = new ArretAgoraphobe();
    public static Arret calme = new ArretCalme();
    public static Arret nerveux = new ArretNerveux();
    public static Arret prudent = new ArretPrudent();
    public static Arret poli = new ArretPoli();

    public static Arret createArretAgoraphobe() {
        return agoraphobe;
    }

    public static Arret createArretCalme() {
        return calme;
    }

    public static Arret createArretNerveux() {
        return nerveux;
    }

    public static Arret createArretPrudent() {
        return prudent;
    }

    public static Arret createArretPoli() {
        return poli;
    }

}
