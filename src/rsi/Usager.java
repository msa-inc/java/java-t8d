package rsi;

public interface Usager {
    public String nom();
    public void monterDans(Autobus t);
}